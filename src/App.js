import ContentComponent from "./components/content/ContentComponent";
import FooterComponent from "./components/footer/FooterComponent";
import HeaderComponent from "./components/header/HeaderComponent";

function App() {
  return (
    <div>
      <HeaderComponent/>
      <ContentComponent/>
      <FooterComponent/>
    </div>
  );
}

export default App;
