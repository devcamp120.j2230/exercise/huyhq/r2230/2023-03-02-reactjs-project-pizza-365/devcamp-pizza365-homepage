import { Container } from "@mui/material";
import FooterContent from "./FooterContent";

const FooterComponent = ()=>{
    return(
        <Container maxWidth={false} disableGutters>
            <FooterContent/>
        </Container>
    );
};

export default FooterComponent