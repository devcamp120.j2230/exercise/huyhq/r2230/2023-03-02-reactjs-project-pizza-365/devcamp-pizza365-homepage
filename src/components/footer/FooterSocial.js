import { faFacebook, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Link, Typography } from "@mui/material";

const FooterSocial = () => {
    return (
        <Grid marginY={3}>
            <Link href="#" color="inherit" mx={1}>
                <FontAwesomeIcon icon={faFacebook} size="1x" />
            </Link>
            <Link href="#" color="inherit" mx={1}>
                <FontAwesomeIcon icon={faYoutube} size="1x" />
            </Link>
            <Link href="#" color="inherit" mx={1}>
                <FontAwesomeIcon icon={faTwitter} size="1x" />
            </Link>
        </Grid>
    );
}

export default FooterSocial;