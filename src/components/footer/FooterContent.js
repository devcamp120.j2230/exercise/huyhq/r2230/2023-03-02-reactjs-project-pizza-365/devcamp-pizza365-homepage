import { faArrowUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Container, Grid, Link, Typography } from "@mui/material"
import FooterSocial from "./FooterSocial"

const FooterContent = () => {
    return (
        <Container maxWidth={false} disableGutters sx={{background: "#ffc107", padding: "50px"}}>
            <Grid container alignSelf="center">
                <Grid item xs={12} textAlign="center">
                    <Typography variant="h6" component="div" marginBottom={3}>Footer</Typography>
                    <Link href="#" color="inherit" underline="none" padding={1} sx={{background: "#343a40", color:"#ffff"}}>
                        <FontAwesomeIcon icon={faArrowUp}/>
                        <Typography variant="span" component="span" ml={1}>To the top</Typography>
                    </Link>
                    <FooterSocial />
                    <Typography variant="p" component="div">Hoàng Quốc Huy</Typography>
                </Grid>
            </Grid>

        </Container>
    )
};

export default FooterContent;