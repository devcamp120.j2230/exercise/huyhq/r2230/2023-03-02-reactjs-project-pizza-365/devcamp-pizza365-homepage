import { Container, createTheme, Grid, Link, ThemeProvider } from "@mui/material";

const HeaderNavComponent = () => {
    return (
            <Container maxWidth={false} sx={{padding: "20px", fontSize: "20px", backgroundColor:  "#ffc107"}}>
                <Grid container textAlign="center" alignSelf="center">
                    <Grid item xs={3}>
                        <Link href="#" underline="none" color="black">Trang chủ</Link>
                    </Grid>
                    <Grid item xs={3}>
                        <Link href="#" underline="none" color="black">Combo</Link>
                    </Grid>
                    <Grid item xs={3}>
                        <Link href="#" underline="none" color="black">Loại Pizza</Link>
                    </Grid>
                    <Grid item xs={3}>
                        <Link href="#" underline="none" color="black">Gửi đơn hàng</Link>
                    </Grid>
                </Grid>
            </Container>
    );
};

export default HeaderNavComponent;