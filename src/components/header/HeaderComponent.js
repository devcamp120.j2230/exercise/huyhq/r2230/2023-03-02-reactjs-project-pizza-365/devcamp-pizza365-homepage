import { Container } from "@mui/material";
import HeaderNavComponent from "./HeaderNavComponent";

const HeaderComponent = ()=>{
    return(
        <Container maxWidth={false} disableGutters>
            <HeaderNavComponent/>
        </Container>
    );
};

export default HeaderComponent;
