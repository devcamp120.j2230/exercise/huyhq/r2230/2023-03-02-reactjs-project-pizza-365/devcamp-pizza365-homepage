import { Box, Container, Typography } from "@mui/material";

const ContentInfo = () => {
    const content = {
        padding: "40px",
        border: "1px solid #ffc107",
        maxHeight: "100%"
    }
    return (
        <Container maxWidth={false} disableGutters sx={{ margin: "50px 0" }}>
            <Typography
                variant="h4"
                component="p"
                sx={{
                    color: "#ffc107",
                    textDecoration: "underline"
                }}
                textAlign="center"
            >
                Tại sao lại Pizza 365
            </Typography>
            <Box
                sx={{ 
                    display: "flex",
                    margin: "20px 0"
                 }}
            >
                <Box
                    xs={3} sm={12} md={3}
                    alignSelf="top"
                    textAlign="left"
                    sx={content}
                    bgcolor="lightgoldenrodyellow"
                >
                    <Typography variant="h4" component="p" my={3}>Đa dạng</Typography>
                    <Typography variant="p" component="p">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</Typography>
                </Box>
                <Box
                    xs={3} sm={12} md={3}
                    alignSelf="top"
                    textAlign="left"
                    sx={content}
                    bgcolor="yellow"
                >
                    <Typography variant="h4" component="p" my={3}>Chất lượng</Typography>
                    <Typography variant="p" component="p">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</Typography>
                </Box>
                <Box
                    xs={3} sm={12} md={3}
                    alignSelf="top"
                    textAlign="left"
                    sx={content}
                    bgcolor="lightsalmon"
                >
                    <Typography variant="h4" component="p" my={3}>Hương vị</Typography>
                    <Typography variant="p" component="p">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</Typography>
                </Box>
                <Box
                    xs={3} sm={12} md={3}
                    alignSelf="top"
                    textAlign="left"
                    sx={content}
                    bgcolor="#ffc107"
                >
                    <Typography variant="h4" component="p" my={3}>Dịch vụ</Typography>
                    <Typography variant="p" component="p">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</Typography>
                </Box>
            </Box>
        </Container >
    )
}

export default ContentInfo;