import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import img1 from "../../asset/images/1.jpg";
import img2 from "../../asset/images/2.jpg";
import img3 from "../../asset/images/3.jpg";
import img4 from "../../asset/images/4.jpg";
import { Container } from "@mui/material";
const ContentLading = () => {
    const imgStyle = {
        width:"100%", 
        height: "auto",
        objectFit: "contain"
    }
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    return (
        <Container maxWidth={false} disableGutters>
            <Slider {...settings} style={{margin: "30px 0"}}>
                <div>
                    <img src={img1} alt="imgSlide" style={imgStyle}/>
                </div>
                <div>
                    <img src={img2} alt="imgSlide" style={imgStyle}/>
                </div>
                <div>
                    <img src={img3} alt="imgSlide" style={imgStyle}/>
                </div>
                <div>
                    <img src={img4} alt="imgSlide" style={imgStyle}/>
                </div>
            </Slider>
        </Container>
    );
}

export default ContentLading;