import { Container, Typography } from "@mui/material"
import ContentInfo from "./ContentInfo";
import ContentLading from "./ContentLading";
import PizzaSize from "./PizzaSize";

const ContentComponent = () => {
    return (
        <Container maxWidth="lg">
            <Typography
                variant="h3"
                component="div"
                sx={{
                    textTransform: "uppercase",
                    color: "#ffc107"
                }}
                marginTop={5}
            >
                PIZZA 365
            </Typography>
            <Typography
                variant="h6"
                component="i"
                sx={{
                    textTransform: "capitalize",
                    color: "#ffc107"
                }}
                marginBottom={5}
            >
                Truly italian!
            </Typography>

            <ContentLading/>
            <ContentInfo/>
            <PizzaSize/>
        </Container>
    )
}

export default ContentComponent;