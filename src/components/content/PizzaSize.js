import { Box, Container, Typography, CardContent, Card, CardActions, Button } from "@mui/material";

const PizzaSize = () => {
    return (
        <Container maxWidth={false} disableGutters sx={{ margin: "50px 0" }}>
            <Typography
                variant="h4"
                component="p"
                sx={{
                    color: "#ffc107",
                    textDecoration: "underline"
                }}
                textAlign="center"
            >
                Chọn size pizza
            </Typography>
            <Typography
                variant="p"
                component="p"
                color="orange"
                textAlign="center"
            >
                Chọn combo pizza phù hợp với nhu cầu của bạn.
            </Typography>
            <Box
                sx={{
                    display: "flex",
                    margin: "20px 0"
                }}
            >
                <Box
                    xs={4} sm={12} md={4}
                >
                    <Card sx={{ minWidth: 275 }}>
                        <CardContent>
                            <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                                Word of the Day
                            </Typography>
                            <Typography variant="h5" component="div">
                                be
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                                adjective
                            </Typography>
                            <Typography variant="body2">
                                well meaning and kindly.
                                <br />
                                {'"a benevolent smile"'}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button size="small">Learn More</Button>
                        </CardActions>
                    </Card>
                </Box>
            </Box>
        </Container >
    )
};

export default PizzaSize;